# Control de Donaciones y Empresas Donantes (code)

Sistema que ayuda a llevar el control de las cadenas de donantes de alimentos y sus productos donados para poder gestionar de manera eficiente y equitativa los productos recolectados.

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```


### Format the files
```bash
yarn format
# or
npm run format
```



### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).
